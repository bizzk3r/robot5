//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//all robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //in this function change levelName
    override func viewDidLoad() {
        levelName = "L666H" // level name
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // зебра
        
        
        // проверяем высоту мира шагами
        var width = checkWidth() + 1
        
        //запускаем по очереди линию с конфетами, переход, линию без конфет, переход, уменьшая на каждом
        // шаге значение высоты
        
        while width>0 {
            makeLineWithCandy(true)
            transition()
            width-=1
            makeLineWithCandy()
            transition()
            width-=1
            
        }
    }
    //переход на следующую строку в зависимости от направления робота
    func transition (){
        if facingRight {
            turnRight()
            if frontIsClear {
                move()}
            turnRight()
        } else {
            turnLeft()
            if frontIsClear {
                move()}
            turnLeft()
        }
    }
    
    // делает линию с конфетами или без в зависимости от параметра
    func makeLineWithCandy (_ candy:Bool = false){
        if candy {
            if frontIsBlocked {                           // обработка для L666H
                put()
                
            }
            while frontIsClear {
                if noCandyPresent{
                    put()}
                move()
                put()
                
            }
            
        } else {
            if frontIsClear {                              // обработка для L666H
                while frontIsClear {
                    move()
                }
            }
        }
    }
    
    
    
    
    // проверка ширины (высоты) мира и возврат
    func checkWidth () ->Int {
        turnRight()
        var res=0
        while frontIsClear {
            move()
            res+=1
        }
        turnAround()
        while frontIsClear{
            move()
        }
        turnRight()
        return res
    }
    
    
    func turnAround(){
        turnRight()
        turnRight()
    }
    
    func turnLeft (){
        turnRight()
        turnRight()
        turnRight()
    }
    
}



